﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RegexBezao
{
    class RegexUtilities
    {
        static void Main(string[] args)
        {
            static MatchCollection EmailRegex(string text)
            {
                Regex rg = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);
                MatchCollection validEmail = rg.Matches(text);
                return validEmail;
            }

            Console.WriteLine("Please Input the string of email(s) you want to validate separated by a comma(,)or semicolon(;) for multiple emails");
            string text = Console.ReadLine();
            MatchCollection validEmail = EmailRegex(text);
            int emailCount = validEmail.Count;

            if(emailCount > 0)
            {
                Console.WriteLine("{0} valid email(s)\n They are; ", emailCount);
                foreach(var mail in validEmail)
                {
                    Console.WriteLine(mail);
                }
            }
            else
            {
                Console.WriteLine("VALID EMAIL(S) NOT FOUND!... TO CONFIRM EMAIL VALIDITY, RUN TEST EMAILS INSTEAD.");
            }
        }
    }
}